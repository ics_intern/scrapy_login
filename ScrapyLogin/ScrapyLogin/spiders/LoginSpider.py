import scrapy
from scrapy import Spider
from http import cookies
from scrapy.http import FormRequest


class LoginSpiderSpider(Spider):
    name = "LoginSpider"



    def start_requests(self):
        login_url = 'http://quotes.toscrape.com/login'
        yield scrapy.Request(login_url, callback=self.login)

    def login(self, response):
        token = response.css("form input[name=csrf_token]::attr(value)").extract_first()
        yield FormRequest.from_response(response,
                                         formdata={'csrf_token': token,
                                                   'password': 'foobar',
                                                   'username': 'foobar'},
                                         callback=self.start_scraping)


    def start_scraping(self, response):
        ## Insert code to start scraping pages once logged in
        for quote in response.css('div.quote'):
            yield {
                'text':quote.css('span.text::text').get(),
                'author':quote.css('small.author::text').get(),
                'tags':quote.css('div.tags a.tags::text').getall(),
            }